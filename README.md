# Bank account kata

This project is created with Spring Boot.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Clone this repository

```
git clone https://bitbucket.org/hdr46/bank-account-kata.git
```

Launch the project locally, by running the main class BankAccountKataApplication.java.

or by using the Maven command line:

1) First, execute the Maven package phase in order to generate the jar file:

```
mvn package
```

2) After, launch the generated jar file by the java command line:

```
java -jar target/bankAccountKata-0.0.1-SNAPSHOT.jar
```

To execute the unites tests, launch the Maven test phase:

```
mvn test
```

## REST API

The bank account rest api is launched on http://localhost:8080/ and provides the following services at the point /api/operation (you have a testing account with the id: 100):

1) As a bank client, you can make a deposit in your account, by using the end point /deposit:

With CURL :  

```
curl -d '{"accountId":"100", "amount":"10000.0"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/operation/deposit
```
2) As a bank client, you can make a withdrawal in your account, by using the end point /withdrawal:

```
curl -d '{"accountId":"100", "amount":"5000.0"}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/operation/withdrawal
```

2) As a bank client, you can see the operations history in your account, by using the end point /allAccountOperations/{accountId}:

```
curl -H "Content-Type: application/json" -X GET http://localhost:8080/api/operation/allAccountOperations/100
```

3) you can also request for a page of operations history by specifying the size and the index of the page, by using the end point /allAccountOperations/{accountId}/{page}/{size}

```
curl -H "Content-Type: application/json" -X GET http://localhost:8080/api/operation/allAccountOperations/100/0/2
```