package com.oxiane.bankAccountKata.service;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.service.error.AccountBalanceException;
import com.oxiane.bankAccountKata.service.error.ExistingAccountException;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.error.NonExistingClientException;

public interface AccountService {
	
	/**
	 * get account by id
	 * 
	 * @param accountId
	 * @return retrieved account  
	 * @throws NonExistingAccountException is thrown when the asked account does not exist
	 */
	public Account getAccount(Integer accountId) throws NonExistingAccountException;

	/**
	 * verification of the existence of the account
	 * 
	 * @param accountId
	 * @return true/false 
	 */
	public Boolean isAccountExist(Integer accountId);

	/**
	 * updating account balance
	 * 
	 * @param accountId
	 * @param balance
	 * @return the updated account
	 * @throws NonExistingAccountException is thrown when the account to update does not exist
	 * @throws AccountBalanceException is thrown when the new balance value is false
	 */
	public Account updateAccount(Integer accountId, Double balance) throws NonExistingAccountException, AccountBalanceException;

	/**
	 * create a new account
	 * 
	 * @param account
	 * @return the created account
	 * @throws ExistingAccountException is thrown when the account to create already exist
	 * @throws NonExistingClientException is thrown when the client of the account does not exist
	 */
	public Account creatAccount(Account account) throws ExistingAccountException, NonExistingClientException;
}
