package com.oxiane.bankAccountKata.service.error;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

/**
 * 
 * overloading the Httpstatus class in order to introduce the information about
 * the entity and the error coding
 *
 */
@JsonFormat(shape = Shape.OBJECT)
public enum HttpErrorCode {
	// XXXYYZZ: XXXX http status, YY entities (ex:CLient, Account, Operation....),
	// ZZ error code
	// for instance 4000301 code is composed by 400(Bad Request) 03 for the entity
	// operation 01 for the error Bad operation balance error

	BAD_REQUEST_GLOBAL(4000000, "Bad request"),
	BAD_REQUEST_GLOBAL_VALIDATION(4000001, "Bad request, faileds validation error"),
	BAD_REQUEST_GLOBAL_PARAMETER_MISSING(4000002, "Bad request, parameters is missing error"),
	BAD_REQUEST_GLOBAL_CONSTRAIN_VIOLATION(4000003, "Bad request, constrain is violated"),
	BAD_REQUEST_GLOBAL_METHOD_ARGUMENT_TYPE_MISMATCH(4000004, "Bad request, method argument is not the expected type"),
	BAD_REQUEST_GLOBAL_NO_HANDLER_FOUND(4000005, "Bad request, no Handler Found"),

	NONE_EXISTING_CLIENT(4000100, "None existing client"), EXISTING_CLIENT(4000101, "Existing cLient"),

	NONE_EXISTING_ACCOUNT(4000200, "None existing Account"), EXISTING_ACCOUNT(4000201, "Existing Account"),

	BAD_OPERATION_AMOUNT(4000300, "Bad operation amount"), BAD_OPERATION_BALANCE(4000301, "Bad operation balance"),
	INSUFFICIENT_FUNDS_OPERATION(4000302, "Insufficient funds for the operation"),

	INTERNAL_SERVER_ERROR_GLOBAL(5000000, "Internal server error");

	private int value;
	private String reasonPhrase;

	HttpErrorCode(int value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getReasonPhrase() {
		return reasonPhrase;
	}

	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}

	public static HttpErrorCode resolve(int httpErrorCodevalue) {
		for (HttpErrorCode httpErrorCode : values()) {
			if (httpErrorCode.value == httpErrorCodevalue) {
				return httpErrorCode;
			}
		}
		return null;
	}

	public HttpStatus getHttpStatus() {
		int httpstatusCode = this.value / 10000;
		return HttpStatus.valueOf(httpstatusCode);
	}

	@Override
	public String toString() {
		return this.value + " " + name();
	}
}
