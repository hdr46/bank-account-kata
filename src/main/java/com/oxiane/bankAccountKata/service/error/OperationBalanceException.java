package com.oxiane.bankAccountKata.service.error;

public class OperationBalanceException extends RuntimeException{
	
	private static final String errorMessage = "The balance is not correct for the operation";
	
	public OperationBalanceException() {
		super(errorMessage);
	}
}
