package com.oxiane.bankAccountKata.service.error;

public class OperationAmountException extends RuntimeException{

	private static final String errorMessage = "The operation amount is not correct";
	
	public OperationAmountException() {
		super(errorMessage);
	}
}
