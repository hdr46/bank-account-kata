package com.oxiane.bankAccountKata.service.error;

public class NonExistingAccountException extends RuntimeException{

	private static final String errorMessage = "There is no account with the id";
	
	public NonExistingAccountException() {
		super(errorMessage);
	}

	public NonExistingAccountException(Integer id) {
		super(errorMessage + " : " + id);
	}
}
