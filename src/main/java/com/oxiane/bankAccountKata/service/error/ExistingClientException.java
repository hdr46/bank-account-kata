package com.oxiane.bankAccountKata.service.error;

public class ExistingClientException extends RuntimeException{
	
	private static final String errorMessage = "There is already a client with the same id";

	public ExistingClientException() {
		super(errorMessage);
	}

	public ExistingClientException(Integer id) {
		super(errorMessage + " : " + id);
	}
}
