package com.oxiane.bankAccountKata.service.error;

public class NonExistingClientException extends RuntimeException{

	private static final String errorMessage = "There is no client with the id";
	
	public NonExistingClientException() {
		super(errorMessage);
	}

	public NonExistingClientException(Integer id) {
		super(errorMessage + " : " + id);
	}
}
