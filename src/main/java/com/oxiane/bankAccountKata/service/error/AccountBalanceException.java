package com.oxiane.bankAccountKata.service.error;

public class AccountBalanceException extends RuntimeException {

	private static final String errorMessage = "The balance is not correct for the account with the id";
	
	public AccountBalanceException() {
		super(errorMessage);
	}

	public AccountBalanceException(Integer id) {
		super(errorMessage + " : " + id);
	}
}
