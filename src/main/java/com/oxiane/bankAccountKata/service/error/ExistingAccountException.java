package com.oxiane.bankAccountKata.service.error;

public class ExistingAccountException extends RuntimeException{
	private static final String errorMessage = "There is already an account with the same id";
	
	public ExistingAccountException() {
		super(errorMessage);
	}

	public ExistingAccountException(Integer id) {
		super(errorMessage + " : " + id);
	}
}
