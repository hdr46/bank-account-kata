package com.oxiane.bankAccountKata.service.error;

public class InsufficientFundException extends RuntimeException {
	private static final String errorMessage = "the operation cannot be completed, there are insufficient funds";
	
	public InsufficientFundException() {
		super(errorMessage);
	}

	public InsufficientFundException(Double insufficientFunds) {
		super(errorMessage + " : " + insufficientFunds);
	}
}
