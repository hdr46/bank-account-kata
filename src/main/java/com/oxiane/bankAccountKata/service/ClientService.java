package com.oxiane.bankAccountKata.service;

import com.oxiane.bankAccountKata.model.Client;
import com.oxiane.bankAccountKata.service.error.ExistingClientException;
import com.oxiane.bankAccountKata.service.error.NonExistingClientException;

public interface ClientService {

	/**
	 * get client by id
	 * 
	 * @param idClient
	 * @return Client
	 * @throws NonExistingClientException is thrown when the asked client does not exist
	 */
	public Client getClient(Integer idClient) throws NonExistingClientException;

	/**
	 * create a new client
	 * 
	 * @param client to create 
	 * @return the created client
	 * @throws ExistingClientException is thrown when the client to create already exist
	 */
	public Client creatClient(Client client) throws ExistingClientException;
}
