package com.oxiane.bankAccountKata.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Operation;
import com.oxiane.bankAccountKata.service.error.OperationBalanceException;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.error.OperationAmountException;

public interface OperationService {

	/**
	 * adding a new deposit operation in the account whit id accountId
	 * 
	 * @param accountId
	 * @param amount
	 * @return the created deposit operation
	 * @throws NonExistingAccountException is thrown when the account with accountId does not exist
	 * @throws OperationAmountException is thrown when the amount value is wrong
	 */
	Operation addDepositOperation(Integer accountId, Double amount) throws NonExistingAccountException, OperationAmountException;

	/**
	 * adding a new withdrawal operation in the account whit id accountId
	 * 
	 * @param accountId
	 * @param amount
	 * @return the created withdrawal operation
	 * @throws NonExistingAccountException is thrown when the account with accountId does not exist
	 * @throws OperationAmountException is thrown when the amount value is wrong
	 * @throws OperationBalanceException is thrown when the resulting balance value value is wrong
	 */
	Operation addWithdrawalOperation(Integer accountId, Double amount) throws NonExistingAccountException, OperationAmountException, OperationBalanceException;

	/**
	 * retrieve all operations list of the account with accountId
	 * 
	 * @param accountId
	 * @return the operations List
	 * @throws NonExistingAccountException is thrown when the account with accountId does not exist
	 */
	List<Operation> getAccountOperationsList(Integer accountId) throws NonExistingAccountException;

	/**
	 * retrieve all operations list of the account with accountId on a specific date
	 * 
	 * @param accountId
	 * @param date
	 * @return the operations List
	 * @throws NonExistingAccountException is thrown when the account with accountId does not exist
	 */
	List<Operation> getAccountOperationsListByDate(Integer accountId, Date date) throws NonExistingAccountException;

	/**
	 * retrieve all operations list of the account with accountId with a date between two dates
	 * 
	 * @param accountId
	 * @param startDate
	 * @param endDate
	 * @return the operations List
	 * @throws NonExistingAccountException is thrown when the account with accountId does not exist
	 */
	List<Operation> getAccountOperationsListBetweenTwoDate(Integer accountId, Date startDate, Date endDate) throws NonExistingAccountException;

	/**
	 * retrieve a page of operations list ordered by date for an account
	 * the page is indexed by page parameter and sized by the size parameter
	 * 
	 * @param accountId
	 * @param page zero-based page index
	 * @param size the size of the page to be returned
	 * @return
	 * @throws NonExistingAccountException is thrown when the account with accountId does not exist
	 */
	List<Operation> getAccountOperationsList(Integer accountId, int page, int size) throws NonExistingAccountException;
}
