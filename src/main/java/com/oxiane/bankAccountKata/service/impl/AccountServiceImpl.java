package com.oxiane.bankAccountKata.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.repository.AccountRepository;
import com.oxiane.bankAccountKata.repository.ClientRepository;
import com.oxiane.bankAccountKata.service.AccountService;
import com.oxiane.bankAccountKata.service.error.AccountBalanceException;
import com.oxiane.bankAccountKata.service.error.ExistingAccountException;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.error.NonExistingClientException;

@Service
public class AccountServiceImpl implements AccountService {

	private AccountRepository accountRepository;

	private ClientRepository clientRepository;

	public AccountServiceImpl() {
	}

	@Autowired
	public AccountServiceImpl(AccountRepository accountRepository, ClientRepository clientRepository) {
		this.accountRepository = accountRepository;
		this.clientRepository = clientRepository;
	}

	@Override
	public Account getAccount(Integer accountId) throws NonExistingAccountException {
		// retrieve the account by using the repository, if the account does not exist
		// we throw an exception
		return accountRepository.findById(accountId).orElseThrow(() -> new NonExistingAccountException(accountId));
	}

	@Override
	public Boolean isAccountExist(Integer accountId) {
		return accountRepository.existsById(accountId);
	}

	@Override
	public Account updateAccount(Integer accountId, Double balance)
			throws NonExistingAccountException, AccountBalanceException {
		Account account = getAccount(accountId);
		// the balance value cannot be negative, otherwise we throw an exception
		if (balance < 0) {
			throw new AccountBalanceException(accountId);
		} else {
			account.setBalance(balance);
		}
		return accountRepository.save(account);
	}

	@Override
	public Account creatAccount(Account account) throws ExistingAccountException, NonExistingClientException {
		//check if the account already exists
		if (account != null && account.getId() != null && accountRepository.existsById(account.getId())) {
			throw new ExistingAccountException(account.getId());
		}
		//check if the client of the account exists
		if (!clientRepository.existsById(account.getClient().getId())) {
			throw new NonExistingClientException(account.getClient().getId());
		}
		return accountRepository.save(account);
	}
}