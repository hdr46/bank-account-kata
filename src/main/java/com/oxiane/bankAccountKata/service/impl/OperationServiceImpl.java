package com.oxiane.bankAccountKata.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Operation;
import com.oxiane.bankAccountKata.model.OperationType;
import com.oxiane.bankAccountKata.repository.OperationRepository;
import com.oxiane.bankAccountKata.service.AccountService;
import com.oxiane.bankAccountKata.service.OperationService;
import com.oxiane.bankAccountKata.service.error.OperationBalanceException;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.error.OperationAmountException;

@Service
public class OperationServiceImpl implements OperationService {

	private OperationRepository operationRepository;

	private AccountService accountService;

	public OperationServiceImpl() {
	}

	@Autowired
	public OperationServiceImpl(OperationRepository operationRepository, AccountService accountService) {
		this.operationRepository = operationRepository;
		this.accountService = accountService;
	}

	@Override
	public Operation addDepositOperation(Integer accountId, Double amount)
			throws NonExistingAccountException, OperationAmountException {
		// the operation amount value cannot be negative, otherwise we throw an exception
		if(amount <= 0) {
			throw new OperationAmountException();
		}
		Account account = accountService.getAccount(accountId);
		Operation operation = new Operation(OperationType.DEPOSIT, amount, account.getBalance(), new Date(), account);
		return operationRepository.save(operation);
	}

	@Override
	public Operation addWithdrawalOperation(Integer accountId, Double amount)
			throws NonExistingAccountException, OperationAmountException, OperationBalanceException {
		// the operation amount value cannot be negative, otherwise we throw an exception
		if(amount <= 0) {
			throw new OperationAmountException();
		}
		Account account = accountService.getAccount(accountId);
		Operation operation = new Operation(OperationType.WITHDRAWAL, amount, account.getBalance(), new Date(), account);
		return operationRepository.save(operation);
	}

	@Override
	public List<Operation> getAccountOperationsList(Integer accountId) throws NonExistingAccountException {
		Account account = accountService.getAccount(accountId);
		return operationRepository.findByAccountOrderByDateDesc(account);
	}

	@Override
	public List<Operation> getAccountOperationsListByDate(Integer accountId, Date date)
			throws NonExistingAccountException {
		Account account = accountService.getAccount(accountId);
		return operationRepository.findByAccountAndDateOrderByDateDesc(account, date);
	}

	@Override
	public List<Operation> getAccountOperationsListBetweenTwoDate(Integer accountId, Date startDate, Date endDate)
			throws NonExistingAccountException {
		Account account = accountService.getAccount(accountId);
		return operationRepository.findByAccountAndDateBetweenOrderByDateDesc(account, startDate, endDate);
	}

	@Override
	public List<Operation> getAccountOperationsList(Integer accountId, int page, int size)
			throws NonExistingAccountException {
		Account account = accountService.getAccount(accountId);
		return operationRepository.findByAccountOrderByDateDesc(account,
				PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "date")));
	}
}
