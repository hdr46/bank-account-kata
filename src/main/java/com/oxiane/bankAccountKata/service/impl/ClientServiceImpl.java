package com.oxiane.bankAccountKata.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oxiane.bankAccountKata.model.Client;
import com.oxiane.bankAccountKata.repository.ClientRepository;
import com.oxiane.bankAccountKata.service.ClientService;
import com.oxiane.bankAccountKata.service.error.ExistingClientException;
import com.oxiane.bankAccountKata.service.error.NonExistingClientException;

@Service
public class ClientServiceImpl implements ClientService {

	private ClientRepository clientRepository;

	public ClientServiceImpl() {
	}

	@Autowired
	public ClientServiceImpl(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

	public Client getClient(Integer idClient) throws NonExistingClientException {
		return this.clientRepository.findById(idClient).orElseThrow(() -> new NonExistingClientException(idClient));
	}

	@Override
	public Client creatClient(Client client) throws ExistingClientException {
		if (client != null && client.getId() != null && !this.clientRepository.findById(client.getId()).isEmpty()) {
			throw new ExistingClientException(client.getId());
		}
		return this.clientRepository.save(client);
	}
}
