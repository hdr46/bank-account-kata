package com.oxiane.bankAccountKata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.oxiane.bankAccountKata.model.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer>{
	
}
