package com.oxiane.bankAccountKata.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Operation;

public interface OperationRepository  extends CrudRepository<Operation, Integer> {

	/** 
	 * find operations list ordered by date for an account 
	 * 
	 * @param account
	 * @return account operations list
	 */
	List<Operation> findByAccountOrderByDateDesc(Account account);

	/** 
	 * find operations list ordered by date for an account at a specific date
	 * 
	 * @param account
	 * @param date
	 * @return account operations list
	 */
	List<Operation> findByAccountAndDateOrderByDateDesc(Account account, Date date);

	/** 
	 * find a list of operations between two dates ordered by date for an account
	 * 
	 * @param account
	 * @param startDate
	 * @param endDate
	 * @return account operations list
	 */
	List<Operation> findByAccountAndDateBetweenOrderByDateDesc(Account account, Date startDate, Date endDate);

	/**
	 * find a pageable operations list ordered by date for an account
	 * 
	 * @param account
	 * @param pageable
	 * @return account operations list
	 */
	List<Operation> findByAccountOrderByDateDesc(Account account, Pageable pageable);
}
