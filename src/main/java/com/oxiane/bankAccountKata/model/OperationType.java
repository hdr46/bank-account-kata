package com.oxiane.bankAccountKata.model;

public enum OperationType {
	DEPOSIT, WITHDRAWAL;
}
