package com.oxiane.bankAccountKata.model;

import javax.persistence.*;

import com.sun.istack.NotNull;

@Entity
@Table
public class Client {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String name;
	@NotNull
	@Column
	private String email;
	@Column
	private String password;

	public Client() {
	}
	
	public Client(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) { 
            return true; 
        }
		
        if (!(obj instanceof Client)) { 
            return false; 
        }

        return this.getId().equals(((Client)obj).getId()) && this.getEmail().equals(((Client)obj).getEmail());
	}
}
