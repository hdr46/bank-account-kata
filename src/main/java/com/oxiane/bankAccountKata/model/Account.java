package com.oxiane.bankAccountKata.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Account {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column
	@NotNull
	@Min(0)
	private Double balance;
	@ManyToOne
	@NotNull
    private Client client;

	public Account() {
	}

	public Account(Client client, Double balance) {
		this.client = client;
		this.balance = balance;
	}

	public Integer getId() {
		return id;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) { 
            return true; 
        }

        if (!(obj instanceof Account)) { 
            return false; 
        }

		return id.equals(((Account)obj).id) && balance.equals(((Account)obj).balance) && client.equals(((Account)obj).client);
	}
}
