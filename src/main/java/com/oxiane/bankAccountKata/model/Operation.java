package com.oxiane.bankAccountKata.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Operation {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name="operation_type")
	private OperationType operationType;
	@Column
	@NotNull
	@Min(0)
	private Double amount;
	@Column
	@NotNull
	@Min(0)
	private Double balance;
	@Column
	private Date date;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="account_id")
    private Account account;

	public Operation() {
	}

	public Operation(OperationType operationType, Double amount, Double balance, Date date, Account account) {
		this.operationType = operationType;
		this.amount = amount;
		this.balance = balance;
		this.date = date;
		this.account = account;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return this.date + " " + this.operationType + " " + this.amount + " " + this.balance;
	}
}
