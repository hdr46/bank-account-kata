package com.oxiane.bankAccountKata.controller;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class OperationRequest {

	@NotNull
	private Integer accountId;
	@NotNull
	@Min(0)
	private Double amount;

	public OperationRequest() {
	}

	public OperationRequest(Integer accountId, Double amount) {
		this.accountId = accountId;
		this.amount = amount;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
}
