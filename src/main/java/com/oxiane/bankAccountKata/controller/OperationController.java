package com.oxiane.bankAccountKata.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Operation;
import com.oxiane.bankAccountKata.service.AccountService;
import com.oxiane.bankAccountKata.service.OperationService;
import com.oxiane.bankAccountKata.service.error.AccountBalanceException;
import com.oxiane.bankAccountKata.service.error.InsufficientFundException;
import com.oxiane.bankAccountKata.service.error.OperationBalanceException;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.error.OperationAmountException;

@RestController
@RequestMapping("/api/operation")
public class OperationController {

	private OperationService operationService;

	private AccountService accountService;

	public OperationController() {
	}

	@Autowired
	public OperationController(OperationService operationService, AccountService accountService) {
		this.operationService = operationService;
		this.accountService = accountService;
	}

	@PostMapping("/deposit")
	@Transactional
	public ResponseEntity<Operation> addDepositOperation(@RequestBody @Valid OperationRequest operationRequest)
			throws NonExistingAccountException, AccountBalanceException, OperationAmountException {
		Account account = accountService.getAccount(operationRequest.getAccountId());
		// calculate the new resulting balance value by adding the deposit operation
		// amount
		double resultingBalance = account.getBalance() + operationRequest.getAmount();
		// updating the account balance by the new balance value
		account = accountService.updateAccount(operationRequest.getAccountId(), resultingBalance);
		// adding a new deposit operation attached to the current account
		Operation resultinOperation = operationService.addDepositOperation(operationRequest.getAccountId(),
				operationRequest.getAmount());
		return ResponseEntity.status(HttpStatus.CREATED).body(resultinOperation);
	}

	@PostMapping("/withdrawal")
	@Transactional
	public ResponseEntity<Operation> addWithdrawalOperation(@RequestBody @Valid OperationRequest operationRequest)
			throws NonExistingAccountException, OperationAmountException, OperationBalanceException,
			AccountBalanceException, InsufficientFundException {
		Account account = accountService.getAccount(operationRequest.getAccountId());
		// calculate the new resulting balance value by subtracting the deposit
		// operation amount
		double resultingBalance = account.getBalance() - operationRequest.getAmount();
		try {
			// updating the account balance by the new balance value
			account = accountService.updateAccount(operationRequest.getAccountId(), resultingBalance);
		}catch (AccountBalanceException e) {
			// if the account service throws AccountBalanceException, it means that there is 
			// InsufficientFundException exception
			throw new InsufficientFundException(resultingBalance);
		}
		// adding a new deposit operation attached to the current account
		Operation resultinOperation = operationService.addWithdrawalOperation(operationRequest.getAccountId(),
				operationRequest.getAmount());
		return ResponseEntity.status(HttpStatus.CREATED).body(resultinOperation);
	}

	@GetMapping("/accountBalance/{accountId}")
	public ResponseEntity<Double> getAccountBalance(@PathVariable("accountId") @NotNull Integer accountId)
			throws NonExistingAccountException {
		Account account = accountService.getAccount(accountId);
		return ResponseEntity.status(HttpStatus.OK).body(account.getBalance());
	}

	@GetMapping("/allAccountOperations/{accountId}")
	public ResponseEntity<List<Operation>> getAllAccountOperation(@PathVariable("accountId") @NotNull Integer accountId)
			throws NonExistingAccountException {
		List<Operation> operationList = operationService.getAccountOperationsList(accountId);
		return ResponseEntity.status(HttpStatus.OK).body(operationList);
	}

	@GetMapping("/accountOperationsByDate/{accountId}/{date}")
	public ResponseEntity<List<Operation>> getAccountOperationsByDate(@PathVariable("accountId") @NotNull Integer accountId,
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date)
			throws NonExistingAccountException {
		List<Operation> operationList = operationService.getAccountOperationsListByDate(accountId, date);
		return ResponseEntity.status(HttpStatus.OK).body(operationList);
	}

	@GetMapping("/accountOperationsBetweenDates/{accountId}/{startDate}/{endDate}")
	public ResponseEntity<List<Operation>> getAccountOperationsBetweenDates(
			@PathVariable("accountId") @NotNull Integer accountId,
			@PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") @NotNull Date startDate,
			@PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") @NotNull Date endDate)
			throws NonExistingAccountException {
		List<Operation> operationList = operationService.getAccountOperationsListBetweenTwoDate(accountId, startDate,
				endDate);
		return ResponseEntity.status(HttpStatus.OK).body(operationList);
	}

	@GetMapping("/allAccountOperations/{accountId}/{page}/{size}")
	public ResponseEntity<List<Operation>> getAllAccountOperationByPageAndSize(
			@PathVariable("accountId") @NotNull Integer accountId, @PathVariable("page") int page,
			@PathVariable("size") int size) throws NonExistingAccountException {
		List<Operation> operationList = operationService.getAccountOperationsList(accountId, page, size);
		return ResponseEntity.status(HttpStatus.OK).body(operationList);
	}
}
