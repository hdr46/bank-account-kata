--DELETE FROM OPERATION;

DELETE FROM ACCOUNT;
DELETE FROM CLIENT;
DELETE FROM OPERATION;

INSERT INTO CLIENT VALUES (1,'client1', 'client1@oxiane.com', 'secret1'); 
INSERT INTO CLIENT VALUES (2,'client2', 'client2@oxiane.com', 'secret2'); 
INSERT INTO CLIENT VALUES (3,'client3', 'client3@oxiane.com', 'secret3');

INSERT INTO ACCOUNT VALUES (100,14000, 1);

INSERT INTO OPERATION(id, operation_type, amount, balance, date, account_id) VALUES (1,0,5000.0,15000.0,'2021-01-14T00:00:00.00+00:00',100);
INSERT INTO OPERATION(id, operation_type, amount, balance, date, account_id) VALUES (2,0,2000.0,17000.0,'2021-01-15T00:00:00.00+00:00',100);
INSERT INTO OPERATION(id, operation_type, amount, balance, date, account_id) VALUES (3,1,3000.0,14000.0,'2021-01-16T00:00:00.00+00:00',100);  