package com.oxiane.bankAccountKata.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Client;
import com.oxiane.bankAccountKata.model.Operation;
import com.oxiane.bankAccountKata.model.OperationType;
import com.oxiane.bankAccountKata.repository.OperationRepository;
import com.oxiane.bankAccountKata.service.impl.OperationServiceImpl;

@ExtendWith(MockitoExtension.class)
public class OperationServiceTest {

	OperationService operationService;

	@Mock
	AccountService accountService;

	@Mock
	OperationRepository operationRepository;

	private static Integer ACCOUNT_ID = 123;
	
	Client client = new Client("Client1", "client1@oxiane.com", "secret1");
	Account account = new Account(client, 1000d);
	Operation operation1;
	Operation operation2;
	Operation operation3;
	Operation operation4;
	Date date1;
	Date date2;
	Date date3;

	@BeforeEach
	public void setup() {
		operationService = new OperationServiceImpl(operationRepository, accountService);
		Calendar cal = Calendar.getInstance();
		cal.set(2021, Calendar.JANUARY, 13);
		date1 = cal.getTime();
		cal.set(2021, Calendar.JANUARY, 15);
		date2 = cal.getTime();
		cal.set(2021, Calendar.JANUARY, 16);
		date3 = cal.getTime();

		operation1 = new Operation(OperationType.DEPOSIT, 1000d, 2000d, date1 , account);
		operation2 = new Operation(OperationType.DEPOSIT, 3000d, 5000d, date1 , account);
		operation3 = new Operation(OperationType.WITHDRAWAL, 500d, 4500d, date2 , account);
		operation4 = new Operation(OperationType.WITHDRAWAL, 100d, 4400d, date3 , account);
	}

	@DisplayName("test if the service correctly returns the operations list of an account")
	@Test
	void testAddDepositOperation() {
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(operationRepository.save(ArgumentMatchers.any(Operation.class))).thenReturn(operation1);
		Operation resultingOperation = operationService.addDepositOperation(ACCOUNT_ID, 1000d);
		assertEquals(OperationType.DEPOSIT, resultingOperation.getOperationType());
		assertEquals(1000d, resultingOperation.getAmount());
		assertEquals(2000d, resultingOperation.getBalance());
	}

	@DisplayName("test if the service correctly returns the operations list of an account")
	@Test
	void testRetrieveAccountOperationList() {
		List<Operation> expectedOperationList = Arrays.asList(operation1, operation2, operation3);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(operationRepository.findByAccountOrderByDateDesc(account)).thenReturn(expectedOperationList);
		assertIterableEquals(expectedOperationList, operationService.getAccountOperationsList(ACCOUNT_ID));
	}

	@DisplayName("test if the service correctly returns the operations list of an account on a specific date")
	@Test
	void testRetrieveAccountOperationListByDate() {
		List<Operation> expectedOperationList = Arrays.asList(operation1, operation2);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(operationRepository.findByAccountAndDateOrderByDateDesc(account, date1)).thenReturn(expectedOperationList);
		assertIterableEquals(expectedOperationList, operationService.getAccountOperationsListByDate(ACCOUNT_ID, date1));
	}

	@DisplayName("test if the service correctly returns operations the list of an account between two dates")
	@Test
	void testRetrieveAccountOperationListBetweenTwoDates() {
		List<Operation> expectedOperationList = Arrays.asList(operation1, operation2);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(operationRepository.findByAccountAndDateBetweenOrderByDateDesc(account, date2, date3)).thenReturn(expectedOperationList);
		assertIterableEquals(expectedOperationList, operationService.getAccountOperationsListBetweenTwoDate(ACCOUNT_ID, date2, date3));
	}
}
