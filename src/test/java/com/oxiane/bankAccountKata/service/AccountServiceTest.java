package com.oxiane.bankAccountKata.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Client;
import com.oxiane.bankAccountKata.repository.AccountRepository;
import com.oxiane.bankAccountKata.repository.ClientRepository;
import com.oxiane.bankAccountKata.service.error.AccountBalanceException;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.impl.AccountServiceImpl;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

	private static final Integer ACCOUNT_ID = 123;

	private AccountService accountService;

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private ClientRepository clientRepository;

	private Client client;
	
	private Account account;

	@BeforeEach
	void setUp() {
		accountService = new AccountServiceImpl(accountRepository, clientRepository);
		client = new Client("Client1", "client1@oxiane.com", "secret1");
		account = new Account(client, 1000d);
	}

	@DisplayName("test if the service correctly returns correctly an existing account")
	@Test
	void testRetrieveExistingAccount() {
		Mockito.when(accountRepository.findById
				(ACCOUNT_ID)).thenReturn(Optional.of(account));
		assertEquals(account, accountService.getAccount(ACCOUNT_ID));
	}

	@DisplayName("test if the service correctly throws a exception whene trying to retrieve a non existing account")
	@Test
	void testRetrievNonExistingAccount() {
		assertThrows(NonExistingAccountException.class, () -> {accountService.getAccount(ACCOUNT_ID);});
	}

	@DisplayName("test if the service correctly returns true when the account exist")
	@Test
	void testIsAccountExist() {
		Mockito.when(accountRepository.existsById(ACCOUNT_ID)).thenReturn(true);
		assertTrue(accountService.isAccountExist(ACCOUNT_ID));
	}

	@DisplayName("test if the service correctly returns false whene the account does not exist")
	@Test
	void testIsAccountNotExist() {
		assertFalse(accountService.isAccountExist(ACCOUNT_ID));
	}
	
	@DisplayName("test if the service correctly updates an existing account with a correct balance value")
	@Test
	void testUpdatingAnExistingAccount() {
		Mockito.when(accountRepository.findById
				(ACCOUNT_ID)).thenReturn(Optional.of(account));
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		Account updatedAccount = accountService.updateAccount(ACCOUNT_ID, 2000d);
		assertEquals(2000d, updatedAccount.getBalance());
	}

	@DisplayName("test if the service correctly throws an exception whene trying to update a non existing account")
	@Test
	void testUpdatingNonExistingAccount() {	
		assertThrows(NonExistingAccountException.class, () -> {accountService.updateAccount(ACCOUNT_ID, 2000d);});
	}

	@DisplayName("test if the service correctly throws an exception whene trying to update an existing account with a non correct balance value")
	@Test
	void testUpdatingExistingAccountWithWrongBalance() {
		Mockito.when(accountRepository.findById
				(ACCOUNT_ID)).thenReturn(Optional.of(account));
		assertThrows(AccountBalanceException.class, () -> {accountService.updateAccount(ACCOUNT_ID, -1000d);});
	}

	@DisplayName("test if the service correctly create a new account")
	@Test
	void testCreatingNewAccount() {
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		Mockito.when(clientRepository.existsById(account.getClient().getId())).thenReturn(true);
		assertEquals(account, accountService.creatAccount(account));
	}
}
