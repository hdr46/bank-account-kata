package com.oxiane.bankAccountKata.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.oxiane.bankAccountKata.model.Client;
import com.oxiane.bankAccountKata.repository.ClientRepository;
import com.oxiane.bankAccountKata.service.error.NonExistingClientException;
import com.oxiane.bankAccountKata.service.impl.ClientServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {

	private static final Integer CLIENT_ID = 123;

	private ClientService clientService;
	
	@Mock 
	private ClientRepository clientRepository;

	private Client client;

	@BeforeEach
	void setUp() {
		clientService = new ClientServiceImpl(clientRepository);
		client = new Client("Client1", "client1@oxiane.com", "secret1");
	}
	
	@DisplayName("test if the service correctly return an existing client")
	@Test
	void testExcistingClient() {
		Mockito.when(clientRepository.findById
				(CLIENT_ID)).thenReturn(Optional.of(client));
		assertEquals(client, clientService.getClient(CLIENT_ID));
	}

	@DisplayName("test if the service correctly throw a exception for a non existing client")
	@Test
	void testNonExcistingClient() {
		assertThrows(NonExistingClientException.class, () -> {clientService.getClient(CLIENT_ID);});
	}
}
