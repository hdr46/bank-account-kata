package com.oxiane.bankAccountKata.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.oxiane.bankAccountKata.model.Client;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ClientRepositoryTest {

	@Autowired
	private ClientRepository clientRepository;

	Client client = new Client("Client1", "client1@oxiane.com", "secret1");
	
	@BeforeEach
	public void setup() {
		clientRepository.save(client);
	}

	@DisplayName("test if the repository correctly saved the client")
	@Test
	void testRegistrationSuccess() {
		assertEquals(client, clientRepository.findById(client.getId()).get());
	}

	@DisplayName("test if the repository correctly throw a exception for a non found client")
	@Test
	void testExpectedException() {
		assertThrows(NoSuchElementException.class, () -> {clientRepository.findById(1234).get();});
	}
}
