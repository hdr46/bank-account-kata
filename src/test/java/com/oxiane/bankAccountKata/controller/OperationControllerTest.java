package com.oxiane.bankAccountKata.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Client;
import com.oxiane.bankAccountKata.model.Operation;
import com.oxiane.bankAccountKata.model.OperationType;
import com.oxiane.bankAccountKata.service.AccountService;
import com.oxiane.bankAccountKata.service.ClientService;
import com.oxiane.bankAccountKata.service.OperationService;
import com.oxiane.bankAccountKata.service.error.AccountBalanceException;
import com.oxiane.bankAccountKata.service.error.CustomizedResponseEntityExceptionHandler;
import com.oxiane.bankAccountKata.service.error.NonExistingAccountException;
import com.oxiane.bankAccountKata.service.error.OperationAmountException;

@ExtendWith(MockitoExtension.class)
public class OperationControllerTest {

	@LocalServerPort
	private int port;

	private MockMvc mockMvc;

	@Mock
	private ClientService clientService;

	@Mock
	private AccountService accountService;

	@Mock
	private OperationService operationService;

	private OperationController operationController;

	private Client client;

	private Account account;

	private static final Integer ACCOUNT_ID = 123; 

	@BeforeEach
	public void setup() {
		operationController = new OperationController(operationService, accountService);
		mockMvc = MockMvcBuilders.standaloneSetup(operationController)
				.setControllerAdvice(new CustomizedResponseEntityExceptionHandler()).build();
		account = new Account(client, 10000d);
	}

	@DisplayName("test if the controller correctly add a deposit operation")
	@Test
	public void testAddingDepositOperation() throws Exception {
		Double expectedBalance = account.getBalance() + 1000d;
		Account expectedAccount = new Account(client, expectedBalance);
		Operation expectedOperation = new Operation(OperationType.DEPOSIT, 1000d, expectedAccount.getBalance(), new Date(), expectedAccount);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(accountService.updateAccount(ACCOUNT_ID, expectedBalance)).thenReturn(expectedAccount);
		Mockito.when(operationService.addDepositOperation(ACCOUNT_ID, 1000d)).thenReturn(expectedOperation);
		this.mockMvc
				.perform(post("/api/operation/deposit")
						.content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, 1000d)))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isCreated()).andExpect(jsonPath("$.operationType", is("DEPOSIT")))
				.andExpect(jsonPath("$.amount", is(1000.0))).andExpect(jsonPath("$.balance", is(11000.0)));
	}

	@DisplayName("test if the controller return an error response after a deposit operation for non existing account")
	@Test
	public void testAddingDepositOperationForNonExistingAccount() throws Exception {
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenThrow(new NonExistingAccountException(ACCOUNT_ID));
		this.mockMvc
				.perform(
						post("/api/operation/deposit").content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, 1000d)))
								.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("There is no account with the id : 123")))
				.andExpect(jsonPath("$.errorCode.value", is(4000200)));
	}

	@DisplayName("test if the controller return an error response after a deposit operation with a wrong amount value")
	@Test
	public void testAddingDepositOperationWithWrongAmountValue() throws Exception {
		Double expectedBalance = account.getBalance() - 1000d;
		Account expectedAccount = new Account(client, expectedBalance);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(accountService.updateAccount(ACCOUNT_ID, expectedBalance)).thenReturn(expectedAccount);
		Mockito.when(operationService.addDepositOperation(ACCOUNT_ID, -1000d)).thenThrow(new OperationAmountException());
		this.mockMvc
				.perform(post("/api/operation/deposit")
						.content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, -1000d)))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("The operation amount is not correct")))
				.andExpect(jsonPath("$.errorCode.value", is(4000300)));
	}

	@DisplayName("test if the controller correctly add a withdrawal operation")
	@Test
	public void testAddingWithdrawalOperation() throws Exception {
		Double expectedBalance = account.getBalance() - 1000d;
		Account expectedAccount = new Account(client, expectedBalance);
		Operation expectedOperation = new Operation(OperationType.WITHDRAWAL, 1000d, expectedAccount.getBalance(), new Date(), expectedAccount);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(accountService.updateAccount(ACCOUNT_ID, expectedBalance)).thenReturn(expectedAccount);
		Mockito.when(operationService.addWithdrawalOperation(ACCOUNT_ID, 1000d)).thenReturn(expectedOperation);
		this.mockMvc
				.perform(post("/api/operation/withdrawal")
						.content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, 1000d)))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isCreated()).andExpect(jsonPath("$.operationType", is("WITHDRAWAL")))
				.andExpect(jsonPath("$.amount", is(1000.0))).andExpect(jsonPath("$.balance", is(9000.0)));
	}

	@DisplayName("test if the controller return an error response after a withdrawal operation for non existing account")
	@Test
	public void testAddingWithdrawalOperationForNonExistingAccount() throws Exception {
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenThrow(new NonExistingAccountException(ACCOUNT_ID));
		this.mockMvc
				.perform(post("/api/operation/withdrawal")
						.content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, 1000d)))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("There is no account with the id : 123")))
				.andExpect(jsonPath("$.errorCode.value", is(4000200)));
	}

	@DisplayName("test if the controller return an error response after a withdrawal operation with a wrong amount value")
	@Test
	public void testAddingWithdrawalOperationWithWrongAmountValue() throws Exception {
		Double expectedBalance = account.getBalance() + 1000d;
		Account expectedAccount = new Account(client, expectedBalance);
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(accountService.updateAccount(ACCOUNT_ID, expectedBalance)).thenReturn(expectedAccount);
		Mockito.when(operationService.addWithdrawalOperation(ACCOUNT_ID, -1000d)).thenThrow(new OperationAmountException());
		this.mockMvc
				.perform(post("/api/operation/withdrawal")
						.content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, -1000d)))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("The operation amount is not correct")))
				.andExpect(jsonPath("$.errorCode.value", is(4000300)));
	}

	@DisplayName("test if the controller return an error response after a withdrawal operation with a insufficient funds")
	@Test
	public void testAddingWithdrawalOperationWithWrongInsufficientFunds() throws Exception {
		Double expectedBalance = account.getBalance() - 10050d;
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		Mockito.when(accountService.updateAccount(ACCOUNT_ID, expectedBalance)).thenThrow(new AccountBalanceException(ACCOUNT_ID));
		this.mockMvc
				.perform(post("/api/operation/withdrawal")
						.content(parsObjectToJsonString(new OperationRequest(ACCOUNT_ID, 10050d)))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message",
						is("the operation cannot be completed, there are insufficient funds : "
								+ (account.getBalance() - 10050d))))
				.andExpect(jsonPath("$.errorCode.value", is(4000302)));
	}

	@DisplayName("test if the controller correctly returns the account balance value")
	@Test
	public void testGetAccountBalance() throws Exception {
		Mockito.when(accountService.getAccount(ACCOUNT_ID)).thenReturn(account);
		this.mockMvc.perform(get("/api/operation/accountBalance/{accountId}", ACCOUNT_ID)).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$", is(10000.0)));
	}

	@DisplayName("test if the controller correctly returns all the account operations")
	@Test
	public void testGetAllAccountOperation() throws Exception {
		Double expectedBalance1 = 10000d;
		Account expectedAccount = new Account(client, expectedBalance1);
		Operation expectedOperation1 = new Operation(OperationType.DEPOSIT, 500d, expectedAccount.getBalance(),
				new Date(), expectedAccount);

		Double expectedBalance2 = expectedBalance1 + 600d;
		expectedAccount = new Account(client, expectedBalance2);
		Operation expectedOperation2 = new Operation(OperationType.DEPOSIT, 600d, expectedAccount.getBalance(),
				new Date(), expectedAccount);

		Double expectedBalance3 = expectedBalance2 - 100d;
		expectedAccount = new Account(client, expectedBalance3);
		Operation expectedOperation3 = new Operation(OperationType.WITHDRAWAL, 100d, expectedAccount.getBalance(),
				new Date(), expectedAccount);

		Double expectedBalance4 = expectedBalance3 - 700d;
		expectedAccount = new Account(client, expectedBalance4);
		Operation expectedOperation4 = new Operation(OperationType.WITHDRAWAL, 700d, expectedAccount.getBalance(),
				new Date(), expectedAccount);

		Mockito.when(operationService.getAccountOperationsList(ACCOUNT_ID)).thenReturn(
				Arrays.asList(expectedOperation4, expectedOperation3, expectedOperation2, expectedOperation1));

		this.mockMvc.perform(get("/api/operation/allAccountOperations/{accountId}", ACCOUNT_ID)).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(4)))
				.andExpect(jsonPath("$[0].operationType", is(OperationType.WITHDRAWAL.toString())))
				.andExpect(jsonPath("$[0].amount", is(700.0))).andExpect(jsonPath("$[0].balance", is(expectedBalance4)))
				.andExpect(jsonPath("$[1].operationType", is(OperationType.WITHDRAWAL.toString())))
				.andExpect(jsonPath("$[1].amount", is(100.0))).andExpect(jsonPath("$[1].balance", is(expectedBalance3)))
				.andExpect(jsonPath("$[2].operationType", is(OperationType.DEPOSIT.toString())))
				.andExpect(jsonPath("$[2].amount", is(600.0))).andExpect(jsonPath("$[2].balance", is(expectedBalance2)))
				.andExpect(jsonPath("$[3].operationType", is(OperationType.DEPOSIT.toString())))
				.andExpect(jsonPath("$[3].amount", is(500.0))).andExpect(jsonPath("$[3].balance", is(expectedBalance1)));
	}

	public static String parsObjectToJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
