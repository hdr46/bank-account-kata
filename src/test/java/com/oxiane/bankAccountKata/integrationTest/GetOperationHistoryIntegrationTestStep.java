package com.oxiane.bankAccountKata.integrationTest;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.model.Operation;
import com.oxiane.bankAccountKata.service.AccountService;
import com.oxiane.bankAccountKata.service.ClientService;

import antlr.collections.List;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetOperationHistoryIntegrationTestStep {

	private static String URL = "http://localhost:";

	private RequestSpecification request;

	private Response response;

	@LocalServerPort
	private static int port = 8080;

	@Autowired
	ClientService clientService;

	@Autowired
	AccountService accountService;

	Account account;
	Double amount;

	@Given("the client's account exists for operations history")
	public void the_client_account_exists_for_operation_history() {
		account = accountService.getAccount(100);
		request = given().headers("Content-Type", JSON, "Accept", JSON);
	}

	@When("the client wants to see operations list")
	public void get_operations_list() {
		response = request.when().get(URL + port + "/api/operation/allAccountOperations/" + account.getId());
	}

	@Then("the operations list should be {string}")
	public void the_operations_list(String operationsList) {
		response.then().statusCode(HttpStatus.OK.value()).assertThat().body("$.size()", is(3));
		assertEquals(operationsList, Arrays.asList(response.getBody().as(Operation[].class)).toString());
	}
}
