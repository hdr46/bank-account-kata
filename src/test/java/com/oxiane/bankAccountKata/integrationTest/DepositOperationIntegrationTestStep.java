package com.oxiane.bankAccountKata.integrationTest;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxiane.bankAccountKata.controller.OperationRequest;
import com.oxiane.bankAccountKata.model.Account;
import com.oxiane.bankAccountKata.service.AccountService;
import com.oxiane.bankAccountKata.service.ClientService;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DepositOperationIntegrationTestStep {

	private static String URL = "http://localhost:";

	private RequestSpecification request;

	private Response response;

	@LocalServerPort
	private static int port = 8080;

	@Autowired
	ClientService clientService;

	@Autowired
	AccountService accountService;

	Account account;
	Double amount;

	@Given("the client's account exists for deposit operation")
	public void the_client_account_exists() {
		account = accountService.getAccount(100);
		request = given().headers("Content-Type", JSON, "Accept", JSON);
	}

	@When("the client wants to do a deposit operation with amount {double}")
	public void the_client_do_deposit_operation(Double amount) {
		this.amount = amount;
		request = request.body(parsObjectToJsonString(new OperationRequest(account.getId(), amount)));
		response = request.when().post(URL + port + "/api/operation/deposit");
	}

	@Then("the account should have a balance of {double} after the deposit operation")
	public void the_account_should_have_after_deposit_operation(Double balance) {
		response.then().statusCode(HttpStatus.CREATED.value()).assertThat().body("operationType", is("DEPOSIT"))
				.body("amount", equalTo(this.amount.floatValue()))
				.body("balance", equalTo(balance.floatValue()));
	}

	public static String parsObjectToJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}