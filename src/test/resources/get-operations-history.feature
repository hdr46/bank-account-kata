Feature: Operations history

  As a bank client I want to see the operations history.
  
  Scenario: get all operations 
  Given the client's account exists for operations history  
  When  the client wants to see operations list  
  Then  the operations list should be "[Sat Jan 16 01:00:00 CET 2021 WITHDRAWAL 3000.0 14000.0, Fri Jan 15 01:00:00 CET 2021 DEPOSIT 2000.0 17000.0, Thu Jan 14 01:00:00 CET 2021 DEPOSIT 5000.0 15000.0]"