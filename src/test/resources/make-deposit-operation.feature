Feature: Deposit operation

  As a bank client I want to make a deposit in my account.
  
  Scenario: make a successful deposit operation  
  Given the client's account exists for deposit operation
  When  the client wants to do a deposit operation with amount 5000.0   
  Then  the account should have a balance of 19000.0 after the deposit operation