Feature: Withdrawal operation

  As a bank client I want to make a withdrawal in my account.
  
  Scenario: make a successful withdrawal operation  
  Given the client's account exists for withdrawal operation  
  When  the client wants to do a withdrawal operation with amount 3000.0   
  Then  the account should have a balance of 16000.0 after the withdrawal operation